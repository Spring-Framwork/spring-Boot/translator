# Translator
Spring Boot Translator

# Technologies
Java 11  
Spring Boot  
Swagger UI  
H2

# How to run
To run the application you only need to start Spring Boot (Server Port: 8080) and access the link below to access the Translator endpoints: 
[Swagger UI](http://localhost:8080/swagger-ui.html)

# How to access H2 Database
[H2 Console](http://localhost:8080/h2-console)

## H2 Database Configuration
**JDBC URL:** jdbc:h2:mem:klxdb  
**User Name:** klx  
**Password:** password  

